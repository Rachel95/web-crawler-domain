CREATE TABLE web_page(
    id              UUID primary key,
    uri             VARCHAR(2000) UNIQUE NOT NULL,
    indexed         TIMESTAMP NOT NULL DEFAULT now(),
    content         TEXT NOT NULL,
    meta_tags       TEXT,
    title           TEXT,
    canonical_uri   TEXT
);

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX web_page_search_idx ON web_page USING gin (meta_tags gin_trgm_ops, title gin_trgm_ops, uri gin_trgm_ops, canonical_uri gin_trgm_ops);