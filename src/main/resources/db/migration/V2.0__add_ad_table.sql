CREATE TABLE ad_page(
    id              UUID PRIMARY KEY,
    keyword         VARCHAR(254) NOT NULL,
    advert          TEXT NOT NULL
);

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX ad_search_idx ON ad_page USING gin (keyword gin_trgm_ops);