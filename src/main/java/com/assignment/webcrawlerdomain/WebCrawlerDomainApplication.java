package com.assignment.webcrawlerdomain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebCrawlerDomainApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebCrawlerDomainApplication.class, args);
	}

}
